
#!/usr/bin/python
# -*- mode:python; coding:utf-8  tab-width:4 -*-

import sys, Ice
Ice.loadSlice("cannon.ice")
import Cannon
import matrix_utils

class Client(Ice.Application):
    def run(self, argv):
        proxy_frontend = self.communicator().stringToProxy(argv[1])
        frontend = Cannon.FrontendPrx.checkedCast(proxy_frontend)

        A = Cannon.Matrix(2, [1,2,3,4])
        B = Cannon.Matrix(2, [3,5,1,0])
        result = frontend.multiply(A,B)
        print result.data

sys.exit(Client().main(sys.argv))
