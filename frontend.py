#!/usr/bin/python -u
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import sys, time
import math
import Ice
Ice.loadSlice('-I {} cannon.ice'.format(Ice.getSliceDir()))
import Cannon
from threading import Event
from processor import *
from matrix_utils import (matrix_horizontal_shift,
                          matrix_vertical_shift,
                          matrix_split, matrix_join)

class FrontendI(Cannon.Frontend):
    def __init__(self, processors):
        self.__processors = processors
        self.__orden_mat_proc = int(math.sqrt(len(processors)))
        self.__collector = None
        self.__proxy_col = None
        self.__adapter = None

    def create_collector(self):
        self.__collector = CollectorI(self.__orden_mat_proc)
        self.__proxy_col = Cannon.CollectorPrx.checkedCast(self.__adapter.addWithUUID(self.__collector))  

    def destroy_collector(self):
        self.__adapter.remove(self.__proxy_col.ice_getIdentity())
        self.__proxy_col = None

    def multiply(self, A, B, current=None):
        self.__adapter = current.adapter
        self.create_collector()
        #Init para los processor.
        self.init_processors()
        #Inject inicial para los processor.
        self.load_processors(A, B)
        result = self.__collector.get_result()
        self.destroy_collector()
        print result
        return result

    def init_processors(self):
        orden = self.__orden_mat_proc
        for i in range(len(self.__processors)):
            #Left
            if (i%orden)-1 >= 0:
                index_left = i - 1
            else:
                index_left = i + (orden-1)
            #Above
            if i-orden >= 0:
                index_above = i -orden
            else:
                index_above = i + ((orden-1)*orden)
            self.__processors[i].init(i, orden, self.__processors[index_above], self.__processors[index_left], self.__proxy_col)
     
    def load_processors(self, A, B):
        #Troceo y permutaciones para las matrices.
        A_split = Cannon.Matrix(self.__orden_mat_proc, matrix_split(A, A.ncols/self.__orden_mat_proc))
        B_split = Cannon.Matrix(self.__orden_mat_proc, matrix_split(B, B.ncols/self.__orden_mat_proc))
        A_prima = matrix_horizontal_shift(A_split)
        B_prima = matrix_vertical_shift(B_split)
        #Envio.
        for i in range(len(self.__processors)):
            self.__processors[i].injectA(A_prima.data[i], 0)
            self.__processors[i].injectB(B_prima.data[i], 0)

class CollectorI(Cannon.Collector):

    def __init__(self, order):
        self.__order = order
        self.__result = None
        self.__blocks = [None]*(order**2)
        self.__num_blocks = 0
        self.__mutex = Event()
        self.__timeout = None

    def inject(self, index, block, current=None):
        self.__num_blocks = self.__num_blocks + 1
        print self.__num_blocks
        self.__blocks[index] = block
        if self.__num_blocks == self.__order**2:
            self.__result = matrix_join(*self.__blocks)
            self.__mutex.set()

    def get_result(self):
        self.__mutex.wait(self.__timeout)
        return self.__result

    def set_timeout(self, timeout):
        self.__timeout = timeout

class Server(Ice.Application):
    def run(self, argv):
        list_processors = []
        #Proxys de processors
        file = open("proxies.txt", "a+")
        for line in file:
            print "Uno mas:"+line
            processor = self.communicator().stringToProxy(line)
            proxy_proc = Cannon.ProcessorPrx.checkedCast(processor)
            list_processors.append(proxy_proc)
        
        file.close()

        servant = FrontendI(list_processors)

        broker = self.communicator()
        adapter = broker.createObjectAdapter('FrontendAdapter')
        proxy = adapter.add(servant, broker.stringToIdentity("frontend"))
        
        print(proxy)

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

if __name__ == '__main__':
    sys.exit(Server().main(sys.argv))
