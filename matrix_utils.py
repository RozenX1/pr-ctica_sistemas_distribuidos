# -*- mode:python; coding:utf-8; tab-width:4 -*-

import Ice
Ice.loadSlice('-I {} cannon.ice'.format(Ice.getSliceDir()))
import Cannon

import itertools, numpy, math

def printmat(mat):
    print "\n"
    for i in mat.data:
        print i.data

def matrix_multiply(A, B):
    order = A.ncols
    C = Cannon.Matrix(order, [])

    for i, j in itertools.product(xrange(order), repeat=2):
        C.data.append(
            sum(A.data[i * order + k] * B.data[k * order + j] for k in xrange(order))
        )

    return C

def matrix_sum(A, B):
    sum = Cannon.Matrix(A.ncols, [])
    for i in range(0, A.ncols**2):
        sum.data.append(A.data[i]+B.data[i])
    
    return sum

def matrix_horizontal_shift(A):
    A_prima = Cannon.Matrix(A.ncols, [])
    for i in range(A.ncols):
        for j in range(A.ncols):
            A_prima.data.append(A.data[((i+j)%A.ncols)+(i*A.ncols)])
           
    return A_prima

def matrix_vertical_shift(B):
    B_prima = Cannon.Matrix(B.ncols, [])
    for i in range(B.ncols):
        for j in range(B.ncols):
            B_prima.data.append(B.data[((j*B.ncols)+j+(i*B.ncols))%(B.ncols**2)])
            
    return B_prima

def matrix_split(mat, block_order):
    orden_mat_res = mat.ncols/block_order
    num_submatrices = (orden_mat_res)**2
    result = []

    for i in range(orden_mat_res):
        for k in range(orden_mat_res):
            submat = Cannon.Matrix(block_order, [])
            for j in range(block_order):
                incremento_filas = (j+(i*block_order))*mat.ncols
                submat.data.extend(mat.data[(k*block_order)+incremento_filas:((k+1)*block_order)+incremento_filas])
    
            result.append(submat)

    return result  

def matrix_join(*mat):
    orden_submat = mat[0].ncols
    orden_mat = int(math.sqrt(len(mat)))
    list_submats = []
    for i in range(len(mat)):
        submat = numpy.split(numpy.array(mat[i].data), orden_submat)
        list_submats.append(submat)

    data_solucion = []
    for i in range(orden_submat*orden_mat):
        for j in range(orden_mat):
            data_solucion.extend(list_submats[j+((i/orden_submat)*orden_mat)][i%orden_submat])

    return Cannon.Matrix(orden_submat*orden_mat, data_solucion)