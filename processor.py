#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import sys
import Ice
Ice.loadSlice('-I {} cannon.ice'.format(Ice.getSliceDir()))
import Cannon
import matrix_utils

class ProcessorI(Cannon.Processor):
    def init(self, index, order, above, left, target, current = None):
        self.__index = index
        self.__order = order
        self.__above = above
        self.__left = left
        self.__target = target
        self.__dict_MA = {}
        self.__dict_MB = {} 
        self.__result = None 
 
    def injectA(self, A, step, current=None):
        if (not self.__dict_MA.has_key(step)) and step < self.__order:
            self.__dict_MA[step] = A
            if self.__left is not None:
                self.__left.injectA(self.__dict_MA[step], step+1)
            if self.__dict_MB.has_key(step):
                C = matrix_utils.matrix_multiply(self.__dict_MA[step], self.__dict_MB[step])
                if(self.__result is None):
                    self.__result = Cannon.Matrix(A.ncols, [0]*(A.ncols**2))
                self.__result = matrix_utils.matrix_sum(self.__result, C)
                if (len(self.__dict_MB) == self.__order) and (len(self.__dict_MA) == self.__order):
                    self.__target.inject(self.__index, self.__result)

    def injectB(self, B, step, current=None):
        if (not self.__dict_MB.has_key(step)) and step < self.__order:
            self.__dict_MB[step] = B
            if self.__above is not None:
                self.__above.injectB(self.__dict_MB[step], step+1)
            if self.__dict_MA.has_key(step):
                C = matrix_utils.matrix_multiply(self.__dict_MA[step], self.__dict_MB[step])
                if(self.__result is None):
                    self.__result = Cannon.Matrix(B.ncols, [0]*(B.ncols**2))
                self.__result = matrix_utils.matrix_sum(self.__result, C)
                if (len(self.__dict_MB) == self.__order) and (len(self.__dict_MA) == self.__order):
                    self.__target.inject(self.__index, self.__result)

class ServerProc(Ice.Application):
    def run(self, args):
        broker = self.communicator()
        servantProc = ProcessorI()
        
        adapter = broker.createObjectAdapter('ProcessorAdapter')
        proxyProc = adapter.addWithUUID(servantProc)

        file = open("proxies.txt", "a+")
        file.write(str('{}\n'.format(proxyProc)))
        file.close()

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()


if __name__ == '__main__':
    app = ServerProc()
    sys.exit(app.main(sys.argv))
