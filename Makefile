#!/usr/bin/make -f
# -*- mode:makefile -*-

OUT = ./proxies.txt
FND = ./frontend.py
PROC = ./processor.py
F_CONFIG = ./frontend.config
P_CONFIG = ./processor.config
NUMBER = 4

make: clean processors
	sleep 1
	python $(FND) --Ice.Config=$(F_CONFIG) < $(OUT) 

processors: 
	for number in `seq 1 $(NUMBER)` ; do \
		python $(PROC) --Ice.Config=$(P_CONFIG) >> $(OUT) & \
    done

clean:
	rm -f *~ $(OUT) *.pyc f c `seq 0 25`
